CREATE TABLE "clientes"
(
    celular           numeric,
    direccion_cliente text COLLATE pg_catalog."default",
    nombre_cliente    text COLLATE pg_catalog."default",
    numero_documento  numeric NOT NULL,
    tipo_documento    text COLLATE pg_catalog."default",
    ciudad            text COLLATE pg_catalog."default",
    CONSTRAINT "CLIENTE_pkey" PRIMARY KEY (numero_documento)
);
CREATE TABLE "autos"
(
    id_auto numeric,
    marca text COLLATE pg_catalog."default",
    linea text COLLATE pg_catalog."default",
    tipo_aut text COLLATE pg_catalog."default",
    nro_chasis text COLLATE pg_catalog."default"
);
CREATE TABLE "vendedor"
(
    celular             numeric,
    direccion_vendedor  text COLLATE pg_catalog."default",
    nombre_vendedor     text COLLATE pg_catalog."default",
    numero_documento    numeric NOT NULL,
    tipo_documento      text COLLATE pg_catalog."default",
    ciudad              text COLLATE pg_catalog."default",
    CONSTRAINT "VENDEDOR_pkey" PRIMARY KEY (numero_documento)
);
CREATE TABLE "seguro"
(
    id_seguro           numeric,
    id_auto             numeric,
    id_propietario      numeric,
    fecha_expedicion    date,
    fecha_vencimiento   date,
    CONSTRAINT "SEGURO_pkey" PRIMARY KEY (id_seguro),
    CONSTRAINT "AUTO_SEGURO_fkey" FOREIGN KEY (id_auto) REFERENCES autos(id_auto) ON DELETE RESTRICT ON UPDATE RESTRICT,
    CONSTRAINT "CLIENTE_SEGURO_fkey" FOREIGN KEY (id_propietario) REFERENCES clientes(numero_documento) ON DELETE RESTRICT ON UPDATE RESTRICT
);