package co.edu.uniajc.backconcesionario_kotlin.service

import co.edu.uniajc.backconcesionario_kotlin.modelo.AutoK
import co.edu.uniajc.backconcesionario_kotlin.repositorio.AutosRepositorioK
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.util.*

@Service
class AutoServiceK {
    @Autowired
    private val autosRepositorioK: AutosRepositorioK? = null
    fun createAuto(autok: AutoK): AutoK {
        return autosRepositorioK!!.save(autok)
    }

    fun ListarAutos(): List<AutoK> {
        return autosRepositorioK!!.findAll() as List<AutoK>
    }

    fun AutoporID(id: Long): Optional<AutoK> {
        return autosRepositorioK!!.findById(id)
    }

    fun DeleteAuto(autoK: AutoK) {
        autosRepositorioK!!.delete(autoK)
    }
}