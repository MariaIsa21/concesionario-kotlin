package co.edu.uniajc.backconcesionario_kotlin.service

import co.edu.uniajc.backconcesionario_kotlin.modelo.AutoK
import co.edu.uniajc.backconcesionario_kotlin.modelo.VendedorK
import co.edu.uniajc.backconcesionario_kotlin.repositorio.AutosRepositorioK
import co.edu.uniajc.backconcesionario_kotlin.repositorio.VendedorRepositorioK
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.util.*

@Service
class VendedorServiceK {
    @Autowired
    private val vendedorRepositorioK: VendedorRepositorioK? = null
    fun createAuto(vendedorK: VendedorK): VendedorK {
        return vendedorRepositorioK!!.save(vendedorK)
    }

    fun ListarAutos(): List<VendedorK> {
        return vendedorRepositorioK!!.findAll() as List<VendedorK>
    }

    fun AutoporID(id: Long): Optional<VendedorK> {
        return vendedorRepositorioK!!.findById(id)
    }

    fun DeleteAuto(vendedorK: VendedorK) {
        vendedorRepositorioK!!.delete(vendedorK)
    }

}