package co.edu.uniajc.backconcesionario_kotlin.repositorio

import co.edu.uniajc.backconcesionario_kotlin.modelo.AutoK
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository
interface AutosRepositorioK : CrudRepository <AutoK, Long> {


}