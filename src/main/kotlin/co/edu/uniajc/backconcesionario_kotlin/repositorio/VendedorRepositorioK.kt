package co.edu.uniajc.backconcesionario_kotlin.repositorio


import co.edu.uniajc.backconcesionario_kotlin.modelo.VendedorK
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository
interface VendedorRepositorioK  : CrudRepository<VendedorK, Long> {
}