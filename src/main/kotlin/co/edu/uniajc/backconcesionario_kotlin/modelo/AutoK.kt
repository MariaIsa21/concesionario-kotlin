package co.edu.uniajc.backconcesionario_kotlin.modelo

import javax.persistence.*

@Entity
@Table(name = "autos")
class AutoK{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_auto")
    private var Id_Auto: Long? = null

    @Column(name = "marca")
    private var Marca: String? = null

    @Column(name = "linea")
    private var Linea: String? = null

    @Column(name = "tipo_aut")
    private var Tipo_Auto: String? = null

    @Column(name = "nro_chasis")
    private var Nro_Chasis: String? = null


    fun AutoK(Id_Auto: Long?, Marca: String?, Linea: String?, Tipo_Auto: String?, Nro_Chasis: String?) {
        this.Id_Auto = Id_Auto
        this.Marca = Marca
        this.Linea = Linea
        this.Tipo_Auto = Tipo_Auto
        this.Nro_Chasis = Nro_Chasis
    }


    fun AutoK() {}

    fun getId_Auto(): Long? {
        return Id_Auto
    }

    fun setId_Auto(id_Auto: Long?) {
        Id_Auto = id_Auto
    }

    fun getMarca(): String? {
        return Marca
    }

    fun setMarca(marca: String?) {
        Marca = marca
    }

    fun getLinea(): String? {
        return Linea
    }

    fun setLinea(linea: String?) {
        Linea = linea
    }

    fun getTipo_Auto(): String? {
        return Tipo_Auto
    }

    fun setTipo_Auto(tipo_Auto: String?) {
        Tipo_Auto = tipo_Auto
    }

    fun getNro_Chasis(): String? {
        return Nro_Chasis
    }

    fun setNro_Chasis(nro_Chasis: String?) {
        Nro_Chasis = nro_Chasis
    }

}