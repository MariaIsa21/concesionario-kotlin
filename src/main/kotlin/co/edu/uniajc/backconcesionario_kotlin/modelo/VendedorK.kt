package co.edu.uniajc.backconcesionario_kotlin.modelo

import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "vendedor")
class VendedorK {
    @Id
    private var Numero_Documento: Long? = null

    private var Tipo_Documento: String? = null
    private var Nombre_Vendedor: String? = null
    private var Celular: Long? = null
    private var Direccion_Vendedor: String? = null
    private var Ciudad: String? = null

    fun Vendedor(
        Numero_Documento: Long?,
        Tipo_Documento: String?,
        Nombre_Vendedor: String?,
        Celular: Long?,
        Direccion_Vendedor: String?,
        Ciudad: String?
    ) {
        this.Numero_Documento = Numero_Documento
        this.Tipo_Documento = Tipo_Documento
        this.Nombre_Vendedor = Nombre_Vendedor
        this.Celular = Celular
        this.Direccion_Vendedor = Direccion_Vendedor
        this.Ciudad = Ciudad
    }

    fun Vendedor() {}

    fun getNumero_Documento(): Long? {
        return Numero_Documento
    }

    fun setNumero_Documento(numero_Documento: Long?) {
        Numero_Documento = numero_Documento
    }

    fun getTipo_Documento(): String? {
        return Tipo_Documento
    }

    fun setTipo_Documento(tipo_Documento: String?) {
        Tipo_Documento = tipo_Documento
    }

    fun getNombre_Vendedor(): String? {
        return Nombre_Vendedor
    }

    fun setNombre_Vendedor(nombre_Vendedor: String?) {
        Nombre_Vendedor = nombre_Vendedor
    }

    fun getCelular(): Long? {
        return Celular
    }

    fun setCelular(celular: Long?) {
        Celular = celular
    }

    fun getDireccion_Vendedor(): String? {
        return Direccion_Vendedor
    }

    fun setDireccion_Vendedor(direccion_Vendedor: String?) {
        Direccion_Vendedor = direccion_Vendedor
    }

    fun getCiudad(): String? {
        return Ciudad
    }

    fun setCiudad(ciudad: String?) {
        Ciudad = ciudad
    }
}