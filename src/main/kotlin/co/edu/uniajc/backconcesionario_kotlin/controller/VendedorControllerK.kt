package co.edu.uniajc.backconcesionario_kotlin.controller

import co.edu.uniajc.backconcesionario_kotlin.modelo.AutoK
import co.edu.uniajc.backconcesionario_kotlin.modelo.VendedorK
import co.edu.uniajc.backconcesionario_kotlin.service.AutoServiceK
import co.edu.uniajc.backconcesionario_kotlin.service.VendedorServiceK
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/api/vendedork")
class VendedorControllerK {
    @Autowired
    var vendedorServiceK: VendedorServiceK? = null

    constructor(service: VendedorServiceK?) {}
    constructor() {}

    @GetMapping
    fun ListartodosAutos(): ResponseEntity<List<VendedorK>> {
        return ResponseEntity.ok(vendedorServiceK!!.ListarAutos())
    }
}