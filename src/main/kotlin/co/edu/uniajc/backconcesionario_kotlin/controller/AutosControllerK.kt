package co.edu.uniajc.backconcesionario_kotlin.controller

import co.edu.uniajc.backconcesionario_kotlin.modelo.AutoK
import co.edu.uniajc.backconcesionario_kotlin.service.AutoServiceK
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/api/autosk")
class AutosControllerK {
    @Autowired
    var autosServiceK: AutoServiceK? = null

    constructor(service: AutoServiceK?) {}
    constructor() {}

    @GetMapping
    fun ListartodosAutos(): ResponseEntity<List<AutoK>> {
        return ResponseEntity.ok(autosServiceK!!.ListarAutos())
    }

}