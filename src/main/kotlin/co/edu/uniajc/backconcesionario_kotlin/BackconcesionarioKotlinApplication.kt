package co.edu.uniajc.backconcesionario_kotlin

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class BackconcesionarioKotlinApplication

fun main(args: Array<String>) {
	runApplication<BackconcesionarioKotlinApplication>(*args)
}
